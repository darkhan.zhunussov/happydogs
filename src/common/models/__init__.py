from typing import Tuple

from reversion import register

from .entity import Entity
from .global_configuration import GlobalConfiguration
from .process import Process, save_and_refresh

register(model=GlobalConfiguration, format='json')

__all__: Tuple = (
    'Entity', 'Process', 'save_and_refresh',
    'GlobalConfiguration'
)
