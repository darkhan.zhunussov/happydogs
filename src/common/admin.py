from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from .models import GlobalConfiguration

admin.site.site_header = _('happydogs_administration')
admin.site.site_title = _('happydogs_administration')
admin.site.index_title = _('happydogs_administration')

admin.site.register(GlobalConfiguration)
