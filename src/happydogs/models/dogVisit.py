from django.db import models
from django.utils.translation import gettext_lazy as _

from common.models import Entity


class DogVisit(Entity):
    dog = models.ForeignKey(
        verbose_name=_('dog'), help_text=_('dog'),
        to='Dog', related_name='dogvisit',
        on_delete=models.SET_NULL, null=True,
    )
    start_date = models.DateField(
        verbose_name=_('start_date'), help_text=_('start_date')
    )
    end_date = models.DateField(
        verbose_name=_('end_date'), help_text=_('end_date')
    )

    class Meta:
        verbose_name = _('dogvisit')
        verbose_name_plural = _('dogvisits')
        db_table = 'happydogs_dogvisit'
