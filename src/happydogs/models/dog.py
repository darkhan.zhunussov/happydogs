from django.db import models
from django.utils.translation import gettext_lazy as _

from common.models import Entity


class Dog(Entity):
    first_name = models.CharField(
        verbose_name=_('first_name'), help_text=_('first_name'),
        max_length=255, unique=True
    )
    last_name = models.CharField(
        verbose_name=_('last_name'), help_text=_('last_name'),
        max_length=255, blank=True, null=True
    )

    class Meta:
        verbose_name = _('dog')
        verbose_name_plural = _('dogs')
        db_table = 'happydogs_dog'
