from typing import Tuple

from happydogs.models.dog import Dog
from happydogs.models.dogVisit import DogVisit

__all__: Tuple = (
    'Dog', 'DogVisit'
)
