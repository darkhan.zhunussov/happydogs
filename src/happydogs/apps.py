from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class happydogsConfig(AppConfig):
    name = 'happydogs'
    verbose_name = _('happydogs')
