from django.views.generic import TemplateView

from happydogs.models import DogVisit


class DogListView(TemplateView):
    template_name = 'dogList.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['response'] = DogVisit.objects.filter(start_date__gte=ctx['date'], end_date__lte=ctx['date'])
        return ctx
