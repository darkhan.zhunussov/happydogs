from datetime import timedelta, datetime

from django.utils import timezone
from django.views.generic import TemplateView

from happydogs.models import DogVisit


class HomePageView(TemplateView):
    template_name = 'homepage.html'

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        today = timezone.now()
        monday = today - timedelta(days=today.weekday())
        ctx['response'] = dict()
        # better to do with aggregate
        for i in range(28):
            day = datetime.strftime(monday + timedelta(days=i), '%Y-%m-%d')
            ctx['response'][day] = DogVisit.objects.filter(start_date__gte=day, end_date__lte=day).count()
        return ctx
