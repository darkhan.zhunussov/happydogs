from typing import Tuple

from happydogs.views.dogList import DogListView
from happydogs.views.homepage import HomePageView


__all__: Tuple = (
    'DogListView', 'HomePageView'
)
