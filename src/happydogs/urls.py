from django.urls import path, re_path
from rest_framework.routers import DefaultRouter

from happydogs.views import HomePageView, DogListView

router = DefaultRouter()
urlpatterns = [
    path('', HomePageView.as_view()),
    re_path(r'^(?P<date>\d{4}-\d{2}-\d{2})/$', DogListView.as_view()),
]
urlpatterns += router.urls
